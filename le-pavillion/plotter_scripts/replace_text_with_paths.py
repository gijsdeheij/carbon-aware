from lxml import etree as ET
import tinycss2
import re
import argparse
import sys

TEXT_ANCHOR_START = 'start'
TEXT_ANCHOR_MIDDLE = 'middle'
TEXT_ANCHOR_END = 'end'

class SVGFontLoader:
    def __init__(self, font):
        self.tree = ET.parse(font)
        self.glyphs = self.load_font()
    
    def load_font(self):
        root = self.tree.getroot()
        namespace = {'svg': 'http://www.w3.org/2000/svg'}
        glyphs = {}

        for glyph in root.findall('.//svg:glyph', namespace):
            unicode_char = glyph.get('unicode')
            d = glyph.get('d')
            horiz_adv_x = glyph.get('horiz-adv-x')
            if unicode_char and d:
                glyphs[unicode_char] = {
                    'path': d,
                    'horiz_adv_x': float(horiz_adv_x) if horiz_adv_x else 0
                }
            elif 'horiz_adv_x':
                glyphs[unicode_char] = {
                    'horiz_adv_x': float(horiz_adv_x) if horiz_adv_x else 0
                }
        return glyphs

    def get_path(self, char):
        return self.glyphs.get(char, {}).get('path', '')

    def get_horiz_adv_x(self, char):
        return self.glyphs.get(char, {}).get('horiz_adv_x', 0)

class SVGTextToPathsConverter:
    def __init__ (self):
        self.default_font_size = 16

    def get_default_space_width(self, font_size):
    # Use a proportion of the average character width as the default space width
        average_char_width = sum(self.font_loader.get_horiz_adv_x(char) for char in 'abcdefghijklmnopqrstuvwxyz') / 26
        # @FIXME not all fonts have 1000 units per em.
        return average_char_width * (font_size / 1000.0)

    def __init__(self, svg_content, font_loader):
        self.svg_content = svg_content
        self.font_loader = font_loader
        
    def calculate_total_line_width(self, text, font_size):
        total_width = 0
        space_width = self.font_loader.get_horiz_adv_x(' ') * (font_size / 1000.0)
        if space_width == 0:
            space_width = self.get_default_space_width(font_size)

        words = text.split()
        for word in words:
            for char in word:
                total_width += self.font_loader.get_horiz_adv_x(char) * (font_size / 1000.0)
            total_width += space_width  # Add space width after each word

        total_width -= space_width  # Remove the last added space width
        return total_width
    
    def get_font_size (self, el):
        if 'font-size' in el.attrib:
            return float(el.attrib['font-size'])
        elif 'style' in el.attrib:
            style = el.attrib['style']
            declarations = tinycss2.parse_blocks_contents(style)
            
            for declaration in declarations:
                if declaration.name == 'font-size':
                    for dimension in declaration.value:
                        if dimension.unit == 'px':
                             return dimension.value
                        else:  
                          return dimension.value

        return None
        
    # Starting point depends on whether text is middle, left or right aligned
    def get_text_anchor (self, el):
        if 'style' in el.attrib:
            style = el.attrib['style']
            declarations = tinycss2.parse_blocks_contents(style)
            for declaration in declarations:
                if declaration.name == 'text-anchor':
                    anchor = declaration.value[0].value
                    if anchor == 'end':
                        return TEXT_ANCHOR_END
                    elif anchor == 'middle':
                        return TEXT_ANCHOR_MIDDLE
                    else:
                        return TEXT_ANCHOR_START
        else:
            return TEXT_ANCHOR_START
    
    # If there is a rotation in the transform.
    # Compensate for the translation in the rotation center point
    def dirty_rotation_fixing (self, transform, x, y):
        # Try to find a ration rotation.
        m = re.match('^rotate\((-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?)\)$', transform)

        if m:
            angle = m.group(1)
            x = float(m.group(2)) - x
            y = float(m.group(3)) - y
            return f'rotate({angle}, {x}, {y})'
        
        return transform

    def convert_tspan_to_path (self, el, fontsize_parent, anchor_parent, x_parent, y_parent):
        text = el.text
        if text is None:
            return None
        
        x = float(el.get('x', None))
        if x is None:
            x = x_parent
        
        y = float(el.get('y', None))
        if y is None:
            y = y_parent
        
        font_size = self.get_font_size(el) # float(el.get('font-size', '16'))
        # Calculate the total width of the text line
        if font_size is None:
            font_size = fontsize_parent
        
        total_line_width = self.calculate_total_line_width(text, font_size)
        text_anchor = self.get_text_anchor(el)
        if text_anchor is None:
            text_anchor = anchor_parent
        
        if text_anchor == TEXT_ANCHOR_MIDDLE: 
            # Adjust the starting position
            x -= total_line_width / 2
        elif text_anchor == TEXT_ANCHOR_END:
            x -= total_line_width

        transform = el.get('transform', None)
        group = ET.Element('g')
        
        if transform:
          transform = self.dirty_rotation_fixing(transform, x, y)
          group.set('transform', f'translate({x},{y}) {transform}')
        else:
          group.set('transform', f'translate({x},{y})')

        current_x = 0

        for char in text:
            path_data = self.font_loader.get_path(char)
            if path_data:
                scaled_path_data = self.scale_path_data(path_data, font_size / 1000.0)
                path_element = ET.Element('path')
                path_element.set('d', scaled_path_data)
                path_element.set('transform', f'translate({current_x},0) scale(1, -1)')

                path_element.set('stroke', 'black')
                path_element.set('stroke-width', '1')
                path_element.set('fill', 'none')

                group.append(path_element)

            current_x += self.font_loader.get_horiz_adv_x(char) * (font_size / 1000.0)

        return group

    def convert_text_to_paths(self):
        # Remove XML declaration
        if self.svg_content.startswith('<?xml'):
            self.svg_content = self.svg_content.split('?>', 1)[1]

        parser = ET.XMLParser(remove_blank_text=True)
        root = ET.fromstring(self.svg_content, parser)
        namespaces = {'svg': 'http://www.w3.org/2000/svg'}

        for text_element in root.findall('.//svg:text', namespaces):
            x = float(text_element.get('x', '0'))
            y = float(text_element.get('y', '0'))
            font_size = self.get_font_size(text_element) # float(text_element.get('font-size', '16'))
            # Calculate the total width of the text line
            text_anchor = self.get_text_anchor(text_element)
            tspans = text_element.findall('./svg:tspan', namespaces)

            if tspans:
                tspan_groups = [self.convert_tspan_to_path(tspan, font_size, text_anchor, x, y) for tspan in tspans]
                parent = text_element.getparent()
                group = ET.Element('g')

                for tspan_group in tspan_groups:
                    group.append(tspan_group)

                parent.remove(text_element)
                parent.append(group)
            else:
                text = text_element.text
                if text is None:
                    continue
                total_line_width = self.calculate_total_line_width(text, font_size)
                if text_anchor == TEXT_ANCHOR_MIDDLE: 
                    # Adjust the starting position
                    x -= total_line_width / 2
                elif text_anchor == TEXT_ANCHOR_END:
                    x -= total_line_width

                transform = text_element.get('transform', None)
                group = ET.Element('g')
                
                if transform:
                  transform = self.dirty_rotation_fixing(transform, x, y)
                  group.set('transform', f'translate({x},{y}) {transform}')
                else:
                  group.set('transform', f'translate({x},{y})')

                current_x = 0

                for char in text:
                    path_data = self.font_loader.get_path(char)
                    if path_data:
                        scaled_path_data = self.scale_path_data(path_data, font_size / 1000.0)
                        path_element = ET.Element('path')
                        path_element.set('d', scaled_path_data)
                        path_element.set('transform', f'translate({current_x},0) scale(1, -1)')

                        path_element.set('stroke', 'black')
                        path_element.set('stroke-width', '1')
                        path_element.set('fill', 'none')

                        group.append(path_element)

                        current_x += self.font_loader.get_horiz_adv_x(char) * (font_size / 1000.0)

                parent = text_element.getparent()
                parent.remove(text_element)
                parent.append(group)

        return ET.tostring(root, pretty_print=True, encoding='unicode')


    def scale_path_data(self, path_data, scale_factor):
        scaled_path = []
        commands = path_data.split()
        for command in commands:
            if command[0] in "MmLlHhVvCcSsQqTtAaZz":
                scaled_path.append(command)
            else:
                coords = command.split(',')
                scaled_coords = ','.join([str(float(coord) * scale_factor) for coord in coords])
                scaled_path.append(scaled_coords)
        return ' '.join(scaled_path)

def replace_text_with_paths (input_svg, output_svg, font):
    if isinstance(input_svg, str):
        with open(input_svg, 'r') as input_svg_h:
            svg_content = input_svg_h.read()
    else:
        # Load the SVG content
        svg_content = input_svg.read()

    # Initialize font loader and text converter
    font_loader = SVGFontLoader(font)
    converter = SVGTextToPathsConverter(svg_content, font_loader)

    # Convert text to paths
    new_svg_content = converter.convert_text_to_paths()

    if output_svg:
        if isinstance(output_svg, str):
            with open(output_svg, 'w') as output_svg_h:
                output_svg_h.write(new_svg_content)
        else:
            output_svg.write(new_svg_content)
    else:
        return new_svg_content

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Replace font', description='Script to replace text and tspan elements in an SVG with paths taken from an SVG stroke font.')
    parser.add_argument('svg_in', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('svg_out', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
    parser.add_argument('--font', type=argparse.FileType('r'), default=open('EMS_test.svg', 'r'))


    args = parser.parse_args()

    replace_text_with_paths(args.svg_in, args.svg_out, args.font)
    # # Example usage
    # input_svg_path = 'file_in.svg'
    # output_svg_path = 'file_output.svg'
    # font_path = 'EMS_test.svg'  # Update this path to your SVG font file

    # replace_text_with_paths(input_svg_path, output_svg_path, font_path)
    # print(f"SVG with text converted to paths saved as {output_svg_path}")