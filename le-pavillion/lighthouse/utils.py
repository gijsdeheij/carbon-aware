from urllib.parse import urlparse
import math

def get_hostname_from_url (url):
    parsed = urlparse(url)
    return parsed.hostname


def human_readable_bytes (bytecount):
    magnitudes = [
        ('giga bytes', 1_000_000_000),
        ('mega bytes', 1_000_000),
        ('kilo bytes', 1_000),
        ('bytes', 1)
    ]

    rest = bytecount
    chunks = []

    for (label, amount) in magnitudes:
        if rest > amount:
            chunks.append('{} {}'.format(math.floor(rest/amount), label))
            rest = rest % amount

    return ', '.join(chunks)