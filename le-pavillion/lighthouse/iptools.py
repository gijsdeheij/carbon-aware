import requests
import socket

# Uses ip-api.com

ip_api_base_url = "http://ip-api.com/json/"

"""
  {
    'status': 'success',
    'country': 'Belgium',
    'countryCode': 'BE',
    'city': 'Woluwe-Saint-Lambert', 
    'lat': 50.842,
    'lon': 4.4383
  }

"""
def geolocate_ip (ip):
  # Return status, country, countryCode, city, lat, lon
  r = requests.get(ip_api_base_url + ip, params={ 'fields': '16595'})

  if r.status_code == 200:
    return r.json()
  else:
    return None
  
def get_public_ip ():
  r = requests.get('https://api.ipify.org')

  if r.status_code == 200:
    return r.text
  else:
    return None

def get_hostname_for_ip (ip):
  try:
    hostname, _, __ = socket.gethostbyaddr(ip)
    return hostname
  except socket.herror:
    return ''

def get_ip_for_hostname (hostname):
  try:
    return socket.gethostbyname(hostname)
  except socket.gaierror:
    return None

if __name__ == '__main__':
  print(geolocate_ip(get_public_ip()))