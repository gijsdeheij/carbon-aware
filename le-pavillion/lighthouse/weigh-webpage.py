from KWHPerByte import KWHDatacenter, KWHDevice, KWHNetwork
from lighthouse import lighthouse
from parse import get_total_transfer_size
from iptools import geolocate_ip, get_ip_for_hostname, get_public_ip
from utils import get_hostname_from_url, human_readable_bytes
from electricity_maps import get_carbon_intensity

url = 'https://www.le-pavillon.be/'

print("Loading {}, and measuring weight using Lighthouse.".format(url))
report = lighthouse(url)

# Parse report
transfer_size = get_total_transfer_size(report)
print('Total bytes transferred: {} bytes -- {}'.format(transfer_size, human_readable_bytes(transfer_size)))

# Convert to KWH
kwh_datacenter = KWHDatacenter(transfer_size)
kwh_network = KWHNetwork(transfer_size)
kwh_device = KWHDevice(transfer_size)

print("\nEnergy use:")
print("   data center: {:f}kWh".format(kwh_datacenter))
print("   network: {:f}Wh".format(kwh_network))
print("   device: {:f}kWh".format(kwh_device))
print("Total: {:f}kWh".format(kwh_datacenter + kwh_network + kwh_device))

# geo locate server and client
client_ip = get_public_ip()

server_hostname = get_hostname_from_url(url)
server_ip = get_ip_for_hostname(server_hostname)

print()
print('Server IP: {}'.format(server_ip))
print('Client IP {}'.format(client_ip))

server_location = geolocate_ip(server_ip)
client_location = geolocate_ip(client_ip)

print()
print('Server located in {}, {}'.format(server_location['city'], server_location['country']))
print('Client located in {}, {}'.format(client_location['city'], client_location['country']))

# Call Electricity Maps
if server_location and server_location['status'] == 'success':
  server_intensity_result = get_carbon_intensity(server_location['lat'], server_location['lon'])

  if server_intensity_result:
    server_intensity = server_intensity_result['carbonIntensity']

if client_location and client_location['status'] == 'success':
  client_intensity_result = get_carbon_intensity(client_location['lat'], client_location['lon'])

  if client_intensity_result:
    client_intensity = client_intensity_result['carbonIntensity']

# Report
print()
print('Carbon intensity at server region: {} gCO2eq/kWh'.format(server_intensity))
print('Carbon intensity at client region: {} gCO2eq/kWh'.format(client_intensity))

co2_server = kwh_datacenter * server_intensity
co2_client = kwh_device * client_intensity
co2_network = kwh_network * .5 * (client_intensity + server_intensity)

print()
print('CO²')
print('   data center: {:f} grams'.format(co2_server))
print('   network: {:f} grams'.format(co2_network))
print('   device: {:f} grams'.format(co2_client))
print('Total: {:f} grams'.format(co2_server + co2_network + co2_client))
print()
print('Per 1000 pages: {:f} grams CO²'.format(1000 * (co2_server + co2_network + co2_client)))