import datetime
import json
from parse_electricity_maps_csv import load_carbon_intensities


"""
  Returns energy usage in kWh for a given time period at given date.
  In this example usage is linked to assumed opening hours 12:00 - 18:00
"""
def get_usage (timedelta, date):
    if date.hour >= 12 and date.hour < 18:
        delta_hours = timedelta.total_seconds() / 3600
        return delta_hours * 9.314 
    else:
        return 0

get_carbon_intensity = load_carbon_intensities('BE_2022_hourly.csv') 


def analyze_production_data (production_data):
    # Production of solar in kWh
    production = 0
    # Time between data points
    timedelta = datetime.timedelta(minutes=5)
    timedelta_hours = timedelta.total_seconds() / 3600

    usage = {
       'total': 0,
       'solar': 0,
       'grid': 0
    }
    co2 = 0

    for [timestamp, production_now] in data:
        date = datetime.datetime.fromtimestamp(timestamp/1000)
        
        # Production is in kWh
        production_now *= timedelta_hours
        
        # Assume the panel produces the same amount of energy until the next measurement
        production += production_now

        # Derive 
        usage_now = get_usage(timedelta, date)
        usage_solar_now = min(usage_now, production_now)
        usage_grid_now = max(0, usage_now - production_now)

        usage['total'] += usage_now
        usage['solar'] += usage_solar_now
        usage['grid'] += usage_grid_now

        co2 += usage_grid_now * get_carbon_intensity(date)

    return {
       'production': production,
       'usage': usage,
       'co2': co2
    }

for (date, path) in [('22 May 2022', 'data-22-5-22.json'), ('23 June 2022', 'data-23-6-22.json')]:
    with open(path, 'r') as h:
        data = json.load(h)
        analysis = analyze_production_data(data)


        print()
        print(date)
        print('Total production during day')
        print('{:.2f} kWh'.format(analysis['production']))

        print('Total energy used')
        print('{:.2f} kWh'.format(analysis['usage']['total']))

        print('Used solar power')
        print('{:.2f} kWh'.format(analysis['usage']['solar']))

        print('Used from the grid')
        print('{:.2f} kWh'.format(analysis['usage']['grid']))

        print('CO2')
        print('{:.2f} gr'.format(analysis['co2']))
