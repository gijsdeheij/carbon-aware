# Carbon Aware API

Interface which shows the current energy mix at the location of the server and of the client. It uses the electricity maps API to retreive the actual energy mix, and the GeoLite api to detect the location of the client and the server.


## Installation

Download this repository.

### Install requirements

Install the required packages using:

```
pip install -r requirements.txt
```

### Set Electricity Map API key

To retreive the current energry mix the interface uses the [free tier](https://api-portal.electricitymaps.com/) of the Electricity Maps API. This API requires an API key.
Firstly obtain such a key by making an account with Electricity maps. Then, set it in the settings of the interface.
Copy the file `local_settings.py.example` to `local_settings.py` and set the variable `ELECTRICITY_MAPS_API_KEY` to your API key.


### Download geolocation data

The interface uses the GeoLite2 Free database to determine the location of the client and server through their IP-address.
The database can be downloaded after making an account on this website: <https://dev.maxmind.com/geoip/geolite2-free-geolocation-data>

Download the database and copy the `.mmdb`-file to the data folder of this repository. Then, update the variable `GEOLITE2_LOCATION` in `local_settings.py`.

### Running the interface

Run the flask application using:

```
flask --app=web_app run
```

For a deployment run the app with a wsgi server like gunicorn, behind a reverse proxy.

TODO: deployment recipe?