<?php

  define("GEOLOCATE_API_URL", "http://ip-api.com/json/");
  define("ELECTRICITY_MAPS_API_URL", "https://api.electricitymap.org/v3/power-breakdown/latest");
  define("ELECTRICITY_MAPS_API_TOKEN", null); // Currently not necessary.
  define("PUBLIC_IP_API_URL", "https://api.ipify.org");

  /**
   * Use ipify to find public ip address of the API
   */
  function get_public_ip () {
    return file_get_contents(PUBLIC_IP_API_URL);
  }

  function get_ip_for_url ($url) {
    $parts = parse_url($url);
    if ($parts['host']) {
      return gethostbyname($parts['host']);
    }
    else {
      return null;
    }
  }

  /**
   * GEOCode given api using the ip-api api.
   */
  function geolocate_ip ($ip) {
    // Only return status, lat and lon
    // $query = http_build_query([ 'fields' => '16576']);
    // Return status, country, countryCode, city, lat, lon
    $query = http_build_query([ 'fields' => '16595']);
    $response = file_get_contents(GEOLOCATE_API_URL . $ip . '?' . $query);
    return json_decode($response, true);
  }

  /**
   * Transforms the power breakdown associative array into a 
   * reverse sorted array of tuples [ type => str, amount => number, percentage => float ]
   */
  function prepare_production_data ($power_breakdown) {
    arsort($power_breakdown);
    $sum = array_sum($power_breakdown);
    $return_data = [];

    foreach ($power_breakdown as $source => $amount) {
      array_push($return_data, [ 
        'type' => $source,
        'amount' => $amount,
        'percentage' => $amount / $sum
      ]);
    }
    
    return $return_data;
  }

  /**
   * Retreive power break down from electricity maps API
   */
  function powerbreakdown_for_location ($lat, $lon) {
    $context = null;

    if (ELECTRICITY_MAPS_API_TOKEN !== null) {
      $opts = [
        'http'=> [
          'method' => "GET",
          'header' => "auth-token: " . ELECTRICITY_MAPS_API_TOKEN . "\r\n"
        ]
      ];
      
      $context = stream_context_create($opts);
    }
    
    $query = http_build_query([ 'lat' => $lat, 'lon' => $lon ]);
    $response = file_get_contents(ELECTRICITY_MAPS_API_URL . '?' . $query, false, $context);
    $data = json_decode($response, true);
    return $data;
  }

  /**
   * Retreive geolocation and current power breakdown for ip address.
   * 
   * Returns a tuple: [ location | null, power_breakdown | null]
   */
  function get_location_and_power_breakdown_for_ip ($ip) {
    if ($ip) {
      // call api's
      $location = geolocate_ip($ip);
      if ($location['status'] == 'success') {
        $power_breakdown = powerbreakdown_for_location($location['lat'], $location['lon']);
      }
      else {
        $power_breakdown = null;
      }
    }
    else {
      $location = null;
      $power_breakdown = null;
    }

    return [ $location, $power_breakdown];
  }

  if (php_sapi_name() == 'cli') {
    // Invoked from a command line
    $ip = get_public_ip();
    $location = geolocate_ip($ip);

    echo($ip);
    echo("\n");

    if ($location['status'] == 'success') {
      echo("Found geo location\n");
      echo($location['lat'] . ', ' . $location['lon'] . "\n");
      $breakdown = powerbreakdown_for_location($location['lat'], $location['lon']);

      foreach ($breakdown['powerConsumptionBreakdown'] as $type => $amount) {
        echo($type);
        echo($amount);
        echo("\n");
      }
    }
    else {
      echo("Could not get locatoin for ip " . $ip);
    }
  }
  else {
    $server_ip = null;
    $client_ip = null;

    // return json object
    
    // Derive client ip, try HTTP_X_REAL_IP before REMOTE_ADDR
    if (array_key_exists('HTTP_X_REAL_IP', $_SERVER) && $_SERVER['HTTP_X_REAL_IP']) {
      $client_ip = $_SERVER['HTTP_X_REAL_IP'];
    }
    elseif (array_key_exists('REMOTE_ADDR', $_SERVER) && $_SERVER['REMOTE_ADDR']) {
      $client_ip = $_SERVER['REMOTE_ADDR'];
    }
    

    // Try to get relevant server IP. If the api is called through
    // a page, try to find IP of that page.
    if (array_key_exists('HTTP_ORIGIN', $_SERVER) || array_key_exists('HTTP_REFERER', $_SERVER)) {
      if (array_key_exists('HTTP_ORIGIN', $_SERVER) && $_SERVER['HTTP_ORIGIN']) {
        $server_ip = get_ip_for_url($_SERVER['HTTP_ORIGIN']);
      }
      elseif ($_SERVER['HTTP_REFERER']) {
        $server_ip = get_ip_for_url($_SERVER['HTTP_REFERER']);
      }
    }
    elseif (array_key_exists('SERVER_ADDR', $_SERVER) && $_SERVER['SERVER_ADDR']) {
      $server_ip = $_SERVER['SERVER_ADDR'];
    }

    [$client_location, $client_power_breakdown] = get_location_and_power_breakdown_for_ip($client_ip);
    [$server_location, $server_power_breakdown] = get_location_and_power_breakdown_for_ip($server_ip);

    $response = [
      'server' => [
        'ip' => $server_ip,
        'geolocation' => $server_location,
        'power_breakdown' => $server_power_breakdown ? prepare_production_data($server_power_breakdown['powerProductionBreakdown']) : null
      ],
      'client' => [
        'ip' => $client_ip,
        'geolocation' => $client_location,
        'power_breakdown' => $client_power_breakdown ? prepare_production_data($client_power_breakdown['powerProductionBreakdown']) : null
      ]
    ];

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json; charset=utf-8');
    echo(json_encode($response));
  }

?>
