from scapy.layers.inet import traceroute as scapy_traceroute

def remove_local_addresses (route):
  return list(filter(lambda ip: False if ip.startswith('10.') or ip.startswith('192.168') else True, route))
    

def traceroute (dst):
  a, _ = scapy_traceroute(dst)

  trace = a.get_trace()

  # {'206.189.8.110': {1: ('192.168.178.1', False), 2: ('213.219.132.31', False), 3: ('212.71.11.53', False), 4: ('212.71.11.226', False), 5: ('80.81.193.141', False), 11: ('206.189.8.110', True)}}
  route = list(trace.values())[0]
  # {1: ('192.168.178.1', False), ... }
  # { ttl: (ip, final_desitionation?), ... }
  hosts = [ hop[0] for hop in route.values() ]

  return hosts