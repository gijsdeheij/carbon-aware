from flask import Flask, request, render_template, jsonify
from geo_ip import get_public_ip, geolocate_ip
from electricity_maps import get_power_breakdown
from traceroute import traceroute, remove_local_addresses
import geoip2.database
import geoip2.errors
from settings import GEOLITE2_LOCATION
import socket
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_cors import cross_origin
from urllib.parse import urlparse

app = Flask(__name__)

limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["100 per hour"],
    storage_uri="memory://",
)

geo_db = geoip2.database.Reader(GEOLITE2_LOCATION)

# >>>     response.country.iso_code
# 'US'
# >>>     response.country.name
# 'United States'
# >>>     response.country.names['zh-CN']
# u'美国'
# >>>
# >>>     response.subdivisions.most_specific.name
# 'Minnesota'
# >>>     response.subdivisions.most_specific.iso_code
# 'MN'
# >>>
# >>>     response.city.name
# 'Minneapolis'
# >>>
# >>>     response.postal.code
# '55455'
# >>>
# >>>     response.location.latitude
# 44.9733
# >>>     response.location.longitude
# -93.2323
def findInGeoDB(ip):
  try:
    return geo_db.city(ip)
  except geoip2.errors.AddressNotFoundError:
    return None
  
def sort_power_breakdown(power):
  return sorted(power['powerConsumptionBreakdown'].items(), key=lambda t: t[1], reverse=True) if power else []

"""
  Serialize a sorted power breakdown. Expects a list of tuples where the first entry is energy type
  and the second the amount of power produced.
"""
def serialize_sorted_power_breakdown(sorted_power_breakdown):
  summed_power = sum([source[1] for source in sorted_power_breakdown])

  return [{
    'type': source[0],
    'production': source[1],
    'production_relative': source[1] / summed_power
  } for source in sorted_power_breakdown]

def get_hostname(ip):
  try:
    hostname, _, __ = socket.gethostbyaddr(ip)
    return hostname
  except socket.herror:
    return ''

def get_ip_address (hostname):
  try:
    return socket.gethostbyname(hostname)
  except socket.gaierror:
    return None

def is_local_ip (ip):
  if ip in ['0.0.0.0', '127.0.0.1'] or ip.startswith('192.168'):
    return True
  
  return False


server_ip = get_public_ip()
server_geolocation = findInGeoDB(server_ip)

@app.route("/")
def index ():
  
  #server_power = get_power_breakdown(server_geolocation['lat'], server_geolocation['lon'])

  server_power = get_power_breakdown(server_geolocation.location.latitude, server_geolocation.location.longitude)

  client_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)

  if client_ip in ['0.0.0.0', '127.0.0.1']:
    client_ip = server_ip

  if client_ip == server_ip:
    client_geolocation = server_geolocation
    client_power = server_power
    route = remove_local_addresses(traceroute('206.189.8.110'))
  else:
    client_geolocation = findInGeoDB(client_ip)
    client_power = get_power_breakdown(client_geolocation.location.latitude, client_geolocation.location.longitude)
    route = remove_local_addresses(traceroute(client_ip))
  
  route_unique = [ip for k, ip in enumerate(route) if ip not in route[:max(0, k)]]    
  route = [ { 'ip': ip, 'geolocation': findInGeoDB(ip), 'hostname': get_hostname(ip) } for ip in route_unique ]

  return render_template('index.html', **{
    'server': {
      'ip': server_ip,
      'hostname': get_hostname(server_ip),
      'geolocation': server_geolocation,
      'power': server_power,
      'sorted_power_breakdown': sort_power_breakdown(server_power)
    },
    'client':  {
      'ip': client_ip,
      'hostname': get_hostname(client_ip),
      'geolocation': client_geolocation,
      'power': client_power,
      'sorted_power_breakdown': sort_power_breakdown(client_power)
    },
    'route': route[::-1]
  })

def serialize_geo_ip_obj (obj):
  return {
    'latitude': obj.location.latitude,
    'longitude': obj.location.longitude,
    'city': obj.city.name,
    'country': {
      'name': obj.country.name,
      'iso_code': obj.country.iso_code
    }
  }


@app.route("/demo")
@limiter.exempt
def demo ():
  return render_template("demo.html")

@app.route("/pattern")
@limiter.exempt
def pattern ():
  return render_template("pattern.html")



@app.route("/api")
@cross_origin()
def api ():
  server_ip = request.args.get('server_ip', None)
  client_ip = request.args.get('client_ip', None)

  if client_ip is None:
    client_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)

  if client_ip is not None and is_local_ip(client_ip):
    client_ip = get_public_ip()

  if server_ip is None:
    origin = request.headers.get('Origin')

    if origin:
      origin_url = urlparse(origin)

      if origin_url.hostname == 'localhost':
        server_ip = client_ip
      else:
        server_ip = get_ip_address(origin)
    else:
      referrer = request.headers.get('Referer')

      if referrer:
        referrer_url = urlparse(referrer)

        if referrer_url.hostname == 'localhost':
          server_ip = client_ip
        else:
          server_ip = get_ip_address(referrer_url.hostname)

  response = {}

  if server_ip is not None:
    server_geolocation = findInGeoDB(server_ip)
    server_powerbreakdown = get_power_breakdown(server_geolocation.location.latitude, server_geolocation.location.longitude)

    response['server'] = {
      'ip': server_ip,
      'hostname': get_hostname(server_ip),
      'geolocation': serialize_geo_ip_obj(server_geolocation),
      'power_breakdown': serialize_sorted_power_breakdown(sort_power_breakdown(server_powerbreakdown))
    }

  if client_ip is not None and not is_local_ip(client_ip):
    client_geolocation = findInGeoDB(client_ip)
    
    if client_geolocation is not None:
      client_powerbreakdown = get_power_breakdown(client_geolocation.location.latitude, client_geolocation.location.longitude)
      
      response['client'] = {
        'ip': client_ip,
        'hostname': get_hostname(client_ip),
        'geolocation': serialize_geo_ip_obj(client_geolocation),
        'power_breakdown': serialize_sorted_power_breakdown(sort_power_breakdown(client_powerbreakdown))
      }

  return jsonify(response)